What directive is used by Node.js in loading the modules it needs?
ans.require

What node.js module contains a method for server creation?
ans. http

What is the method of the http object responsible for creating a server using node.js?
ans. createServer

What method of the response object allows us to set status codes and content types?
ans. writeHead

Where will console.log() output its contents when run in node.js?
ans. node terminal

What property of the request object contians the address' enpoint?
ans. url