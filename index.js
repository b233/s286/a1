let http = require ("http");

let PORT = 3000;

http.createServer( (req, res) => {
	
	if (req.url == "/login"){
		res.writeHead(200, 
		{"Content-Type": "text/html"});
		res.end("This the login page!");
	} else {
		res.writeHead(404, 
		{"Content-Type": "text/plain"})
		res.end("Wrong Page!")
	}

}
).listen(PORT);

console.log("Server Setup is successful!")